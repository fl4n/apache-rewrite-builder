import re

class Url:
    host_pattern = re.compile('^(?:https?://)?((?:.+?\.)?.+?\..+?)/')
    qstring_pattern = re.compile('\?(.*)$')
    path_pattern = re.compile('^([^\?]+)')

    def __init__(self, source):
        self.source = source

    def qstring(self):
        if qstring_test := re.search(Url.qstring_pattern, self.source):
            return qstring_test.group(1)
        else:
            return ""
    def hostname(self):
        if host_test := re.search(Url.host_pattern, self.source): #has hostname
            return host_test.group(1)
        else:
            return ""
    def path(self):
        path = re.sub(Url.host_pattern, "/", self.source)
        path = re.sub(Url.qstring_pattern, "", path)
        return path



url_1 = "https://www.this.com/blah/blah/123?this=567&that=456"
url_2 = "this.com/blah/blah/123"
url_3 = "/this/page"

print(Url(url_1).path())
print(Url(url_1).qstring())
print(Url(url_1).hostname())

print(Url(url_2).path())
print(Url(url_2).qstring())
print(Url(url_2).hostname())

print(Url(url_3).path())
print(Url(url_3).qstring())
print(Url(url_3).hostname())
