"""
TO DO:
 - Process rewrite output using class in processor.py
 - Clean destination, as spaces here will break the server too
"""

import csv
from processor import Url, UrlComponent, clean_output


# def encoding(input_file):
#     enc = cd.detect(
#         open('rewrites.csv', 'r').read().encode()
#     )['encoding'] + "-sig"
#     return enc

with open('rewrites.csv', encoding='utf-8-sig') as f:
    rewrite_map_raw = [{k: v for k, v in row.items()}
        for row in csv.DictReader(f, skipinitialspace=True)]

rewrite_map = []
for rewrite_raw in rewrite_map_raw:
    source = Url(rewrite_raw.get("source"))
    dest = rewrite_raw.get("destination")

    rewrite_map.append(
        {
            "host": source.hostname(),
            "path": source.path(),
            "query_string": source.qstring(),
            "destination": UrlComponent(dest, "dest")
        }
    )

destinations = set()
query_strings = set()
hosts = set()

for rewrite in rewrite_map:
    destinations.add(rewrite.get("destination"))
    hosts.add(rewrite.get("host"))
    query_strings.add(rewrite.get("query_string"))


master_string = ""
for host in hosts:
    for query_string in query_strings:
        for destination in destinations:
            output_string = ""
            paths = []
            for rewrite in rewrite_map:
                if rewrite.get("destination") == destination \
                        and rewrite.get("host") == host \
                        and rewrite.get("query_string") == query_string:
                    paths.append(rewrite.get("path"))
            if paths:
                if host.source:
                    output_string = output_string + \
                        "RewriteCond %{HTTP_HOST} ^" + clean_output(host.rewrite()) + "$ [NC]\n"
                if query_string.source:
                    output_string = output_string + \
                        "RewriteCond %{QUERY_STRING} ^" + clean_output(query_string.rewrite()) + "$ [NC]\n"
                for path in paths[:-1]:
                    output_string = output_string + \
                        "RewriteCond %{REQUEST_URI} ^" + clean_output(path.rewrite()) + "/?$ [NC,OR]\n"
                output_string = output_string + \
                    "RewriteCond %{REQUEST_URI} ^" + clean_output(paths[len(paths)-1].rewrite()) + "/?$ [NC]\n"
                output_string = output_string + \
                    "RewriteRule ^(.*)$ " + destination.rewrite() + " [R=301,L]\n"
                master_string = master_string + output_string + "\n"

print(master_string)
